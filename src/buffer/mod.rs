//! A buffered wrapper around a `Read` type, providing read and peek methods.

mod byte_buffer;

pub use self::byte_buffer::ByteBuffer;
