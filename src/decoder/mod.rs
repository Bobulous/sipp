//! A decoder of bytes into characters.
//! The trait `ByteStreamCharDecoder` can be implemented for different character encodings, and
//! implementations are included for UTF-8, and UTF-16 (in big-endian and little-endian byte order).

mod byte_stream_char_decoder;
mod utf16_be_decoder;
mod utf16_le_decoder;
mod utf8_decoder;

pub use self::byte_stream_char_decoder::ByteStreamCharDecoder;
pub use self::utf16_be_decoder::Utf16BigEndianDecoder;
pub use self::utf16_le_decoder::Utf16LittleEndianDecoder;
pub use self::utf8_decoder::Utf8Decoder;
