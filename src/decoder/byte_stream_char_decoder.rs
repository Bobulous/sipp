use std::io::Error;
use std::io::Read;

use crate::buffer::ByteBuffer;

/**
A trait which promises that a type can wrap a byte stream and decode it into characters, returning
one character at a time.

Note that the implementation may use an internal buffer, so that it may fetch multiple bytes and
decode multiple characters in advance.
*/
pub trait ByteStreamCharDecoder<T: Read> {
    /**
    Wraps the given `Read` type, allowing its bytes to be decoded into characters.
    */
    fn wrap(stream: T) -> Self;

    /**
    Wraps the given `ByteBuffer`, allowing its bytes to be decoded into characters.
    */
    fn wrap_buffer(buffer: ByteBuffer<T>) -> Self;

    /**
    Returns the next character represented by the byte stream. If there are no bytes remaining
    in the input stream then this method will return `None`, otherwise it will return `Some(c)`.

    This method will not (must not) return Unicode surrogate codepoint characters.

    # Errors

    If the byte stream contains a sequence of bytes which do not represent a valid character
    under the assumed character encoding, or if something goes wrong while reading the byte
    stream, then this method will return an `std::io::Error` variant.
    */
    fn decode_char(&mut self) -> Result<Option<char>, Error>;
}
