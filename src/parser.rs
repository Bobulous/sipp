//! A parser which can wrap a character stream and provide methods to read and peek at the stream.

use std::{
    io::{Error, ErrorKind, Read},
    marker::PhantomData,
};

use crate::decoder::ByteStreamCharDecoder;

/**
Provides methods for peeking and reading characters from a character stream.

# Usage

All of the methods either remove characters from the input stream, or peek at what will come
next in the input stream without removing anything. The table below shows the differences in the
methods.

<table>
<caption style="font-weight: bold; font-size: larger">Parser methods compared</caption>
<tbody>
<tr>
    <th scope="col">Method</th>
    <th scope="col" style="max-width: 8em">Removes from input</th>
    <th scope="col" style="max-width: 8em">Error if no input remains?</th>
    <th scope="col" style="max-width: 8em">Return Result payload</th>
</tr>
<tr>
    <th scope="row">read()</th>
    <td>Yes</td>
    <td>Yes</td>
    <td style="font-family: monospace">char</td>
</tr>
<tr>
    <th scope="row">require(char)</th>
    <td>Yes</td>
    <td>Yes</td>
    <td style="font-family: monospace">()</td>
</tr>
<tr>
    <th scope="row">require_str(&str)</th>
    <td>Yes</td>
    <td>Yes</td>
    <td style="font-family: monospace">()</td>
</tr>
<tr>
    <th scope="row">peek()</th>
    <td>No</td>
    <td>No</td>
    <td style="font-family: monospace">Option&lt;char&gt;</td>
</tr>
<tr>
    <th scope="row">peek_sees(char)</th>
    <td>No</td>
    <td>No</td>
    <td style="font-family: monospace">bool</td>
</tr>
<tr>
    <th scope="row">accept(char)</th>
    <td>Maybe</td>
    <td>No</td>
    <td style="font-family: monospace">bool</td>
</tr>
<tr>
    <th scope="row">read_up_to(char)</th>
    <td>Maybe</td>
    <td>No</td>
    <td style="font-family: monospace">String</td>
</tr>
<tr>
    <th scope="row">skip_while(Fn(char)-&gt;bool)</th>
    <td>Maybe</td>
    <td>No</td>
    <td style="font-family: monospace">bool</td>
</tr>
</tbody>
</table>

# Examples

If you have a `Utf8Decoder` providing a stream of characters, then you can use `Parser` to move
through the content of the stream like this:

```
# use sipp::{parser::Parser, decoder::Utf8Decoder,
# decoder::ByteStreamCharDecoder};
# fn main() -> Result<(), std::io::Error> {
# let input = "abc123<*>";
# let decoder = Utf8Decoder::wrap(input.as_bytes());
let mut parser = Parser::wrap(decoder);
# let mut found_alphabetic = false;
match parser.peek()? {
    None => println!("Input is completely empty!"),
    Some(c) => {
        if c.is_ascii_alphabetic() {
            let mut text = String::with_capacity(8);
            text.push(parser.read()?);
            while let Some(c) = parser.peek()? {
                if c.is_ascii_alphabetic() {
                    text.push(parser.read()?);
                } else {
                    break;
                }
            }
#           assert_eq!(&text, "abc");
#           found_alphabetic = true;
            println!("Input starts with alphabetic sequence {}", &text);
        } else if c.is_ascii_digit() {
            println!("Input starts with numeric sequence!");
        } else {
            println!("Input starts with something which is not ASCII alphanumeric!");
        }
    }
}
# assert!(found_alphabetic);
# Ok(())
# }
```

In the above example, `peek()` is used to check whether the next (first) character fits a certain
requirement before we actually do something with it. Once we know that the character is alphabetic
then we use `read()` to remove that character from the character stream, and we add it into a
mutable `String`. Then the while loop uses `peek()` to check for more content, and so long as
the next character is alphabetic it is `read()` and added to the mutable `String`. As soon as
`peek()` spots that the next character is not alphabetic, we break out of the while-loop before
we consume any more characters.

Note that most of the methods in Parser will return `Err(std::io::Error)` if something goes wrong
while trying to fetch characters from the stream. It's recommended that you keep parsing code within
methods that declare a return type of `Result<T, std::io::Error>` (where `T` is any type you like)
so that you can simply use the `?` operator after calls to `Parser` methods which might give you an
error.

*/
#[derive(Debug)]
pub struct Parser<D, R> {
    decoder: D,
    peek: Option<char>,
    read_phantom: PhantomData<R>,
}

impl<D, R> Parser<D, R>
where
    D: ByteStreamCharDecoder<R>,
    R: Read,
{
    /**
    Returns a `Parser` which wraps the given `ByteStreamCharDecoder` type.

    # Examples

    You can't wrap a byte stream directly, because this parser works with characters not bytes. So
    you first need to identify the character encoding and then wrap the byte stream in a suitable
    type which implements `ByteStreamCharDecoder` trait.

    As an example, if you create a byte stream from a hardcoded string, you know that the character
    encoding is UTF-8 (because Rust uses UTF-8 to encode strings into bytes). So in order to create
    a `Parser` you need to first wrap the byte stream with a `Utf8Decoder` like this:

    ```
    # use sipp::{parser::Parser, decoder::Utf8Decoder,
    # decoder::ByteStreamCharDecoder};
    # fn main() -> Result<(), std::io::Error> {
    let input = "Example hardcoded string";
    let decoder = Utf8Decoder::wrap(input.as_bytes());
    let mut parser = Parser::wrap(decoder);
    parser.require_str("Example hardcoded string")?;
    # Ok(())
    # }
    ```

    However, it's unlikely that you'll be working with hardcoded strings, and more likely that
    you'll want to work with files, network connections, etc. So long as the source type implements
    Rust's `Read` trait, you can wrap it in an appropriate decoder and then wrap that in a `Parser`.

    For example, if you know that you're reading a file which has character encoding UTF-16 with
    big-endian byte order, then you could use this:

    ```
    # use sipp::{parser::Parser, decoder::Utf16BigEndianDecoder,
    # decoder::ByteStreamCharDecoder};
    # use std::fs::File;
    # fn main() -> Result<(), std::io::Error> {
    let file = File::open("test_resources/xml_utf16BE_BOM.xml")?;
    // File implements the Read trait, so we can wrap it with a decoder.
    let decoder = Utf16BigEndianDecoder::wrap(&file);
    let mut parser = Parser::wrap(decoder);
    // Skip the byte-order mark first.
    parser.require('\u{FEFF}')?;
    // Now start reading the content proper.
    parser.require_str("<?xml version=\"1.0\" encoding=\"UTF-16\"?>")?;
    # Ok(())
    # }
    ```

    */
    pub fn wrap(decoder: D) -> Parser<D, R> {
        Parser {
            decoder,
            peek: None,
            read_phantom: PhantomData,
        }
    }

    /**
    Returns `true` if the character stream has more content available, or `false` if there is no
    more content available from the character stream.

    # Errors

    If there is some problem while reading from the stream then this method will return an
    `std::io::Error` variant.

    # Examples

    `has_more` is useful when you want to continue reading characters until there is nothing
    left in the character stream.

    For example, if you want to read a list of numbers separated by newline characters, and you
    want to keep going until the character stream has nothing left, then you can use something
    like this:

    ```
    # use sipp::{parser::Parser, decoder::Utf8Decoder,
    # decoder::ByteStreamCharDecoder};
    # fn main() -> Result<(), std::io::Error> {
    # let input = "1\n2\n3\n4\n5\n6\n7\n8";
    # let decoder = Utf8Decoder::wrap(input.as_bytes());
    # let mut parser = Parser::wrap(decoder);
    # let mut gathered_nums = Vec::with_capacity(8);
    while parser.has_more()? {
        if let Some(num) = parser.read_up_to('\n')? {
            println!("Found num \"{}\".", num);
            # gathered_nums.push(num);
        } else {
            println!("Found empty line, ignoring it.");
        }
        // If there is a newline following this number then skip
        // past it.
        parser.accept('\n')?;
    }
    # assert_eq!(gathered_nums, vec!["1", "2", "3", "4", "5", "6",
    #        "7", "8"]);
    # Ok(())
    # }
    ```
    */
    pub fn has_more(&mut self) -> Result<bool, Error> {
        self.peek().map(|o| o.is_some())
    }

    /**
    Returns the next char without removing it from the stream, or `None` if there are no further
    characters in the stream.

    # Errors

    If there is some problem while reading from the stream then this method will return an
    `std::io::Error` variant.

    # Examples

    `peek` is very useful when your code needs to react to the type
    of character found next in the input stream. Because `peek` does not remove anything from the
    stream, you don't interfere with the stream if you don't find the expected character type(s).

    Suppose our code needs to know whether the next portion of the input stream represents an XML
    comment (starting with a "<!--" sequence) or an XML processing instruction (starting with a "<?"
    sequence) or an XML element (starting with a '<' character and not followed by a '!' nor a '?')
    then you can use `peek()` to check the next character before taking action:

    ```
    # use sipp::{parser::Parser, decoder::Utf8Decoder,
    # decoder::ByteStreamCharDecoder};
    # fn main() -> Result<(), std::io::Error> {
    # let input = "<!-- comment found -->";
    # let decoder = Utf8Decoder::wrap(input.as_bytes());
    # let mut parser = Parser::wrap(decoder);
    if let Some('<') = parser.peek()? {
        // Skip past the '<' character.
        parser.read()?;
        // Check what comes after the '<' character.
        match parser.peek()? {
            Some('!') => println!("This is either a comment or a CDATA section!"),
            Some('?') => println!("This is a processing instruction!"),
            Some(_) => println!("Nothing else matches, so this must be an XML element!"),
            None => println!("Input ends suddenly after the '<' character!"),
        }
    } else {
        println!("Some other type of content found!");
    }
    # Ok(())
    # }
    ```

    Note what when checking for a specific character, it's easier to use [`peek_sees`][`Self::peek_sees`] instead.
    */
    pub fn peek(&mut self) -> Result<Option<char>, Error> {
        match self.peek {
            Some(p) => Ok(Some(p)),
            None => match self.decoder.decode_char()? {
                Some(c) => {
                    self.peek = Some(c);
                    Ok(self.peek)
                }
                None => Ok(None),
            },
        }
    }

    // Takes the next character, from the peek buffer if possible,
    // otherwise from the input stream.
    fn read_next(&mut self) -> Result<Option<char>, Error> {
        match self.peek.take() {
            Some(c) => Ok(Some(c)),
            None => self.decoder.decode_char(),
        }
    }

    // Skips the next character, emptying the peek buffer if
    // necessary.
    fn skip_next(&mut self) -> Result<(), Error> {
        match self.peek {
            Some(_) => self.peek.take(),
            None => self.decoder.decode_char()?,
        };
        Ok(())
    }

    /**
    Removes the next character from the stream and returns it.

    # Errors

    If this method is called when the input stream contains no further characters, or if there
    was any other problem while reading from the stream, then this method will return an
    `std::io::Error` variant.

    # Examples

    If you have a character stream which contains the sequence "abcdefg" and nothing more, then you
    could read the characters one at a time like this:

    ```
    # use sipp::{parser::Parser, decoder::Utf8Decoder,
    # decoder::ByteStreamCharDecoder};
    # fn main() -> Result<(), std::io::Error> {
    # let input = "abcdefg";
    # let decoder = Utf8Decoder::wrap(input.as_bytes());
    # let mut parser = Parser::wrap(decoder);
    assert_eq!(parser.read()?, 'a');
    assert_eq!(parser.read()?, 'b');
    assert_eq!(parser.read()?, 'c');
    assert_eq!(parser.read()?, 'd');
    assert_eq!(parser.read()?, 'e');
    assert_eq!(parser.read()?, 'f');
    assert_eq!(parser.read()?, 'g');
    // There's nothing left in the stream, so any further call to `read` will give an error:
    let bad_read = parser.read();
    assert!(bad_read.is_err());
    # Ok(())
    # }
    ```
    */
    pub fn read(&mut self) -> Result<char, Error> {
        match self.read_next()? {
            Some(c) => Ok(c),
            None => Err(Error::new(
                ErrorKind::InvalidData,
                "Expected some character but found end of input.",
            )),
        }
    }

    /**
    Reports on whether the next character in the stream is identical to the specified character,
    without removing anything from the stream. Returns `false` if there are no more characters
    available in the stream.

    # Errors

    If there is any problem while reading from the stream then an `std::io::Error` variant is
    returned.

    # Examples

    Suppose we know that the input stream contains only the character 'Ω' (the Greek character
    capital Omega) then we could use `peek_sees` to check for it without consuming it:

    ```
    # use sipp::{parser::Parser, decoder::Utf8Decoder,
    # decoder::ByteStreamCharDecoder};
    # fn main() -> Result<(), std::io::Error> {
    # let input = "Ω";
    # let decoder = Utf8Decoder::wrap(input.as_bytes());
    # let mut parser = Parser::wrap(decoder);
    // Confirm that the next character in the stream is omega:
    # assert!(parser.peek_sees('Ω')?);
    if parser.peek_sees('Ω')? {
        // Remove it from the stream using `read`:
        let c = parser.read()?;
        assert_eq!(c, 'Ω');
        // Confirm that there's nothing left in the stream:
        assert!(!parser.has_more()?);
        // Confirm that `peek_sees` no longer sees the omega:
        assert!(!parser.peek_sees('Ω')?);
    }
    # Ok(())
    # }
    ```
    */
    pub fn peek_sees(&mut self, c: char) -> Result<bool, Error> {
        match self.peek()? {
            Some(f) => Ok(f == c),
            None => Ok(false),
        }
    }

    /**
    Returns `true` and removes the next character in the stream if it is identical to the given
    character. Otherwise returns `false` and removes nothing from the stream. Returns `false`
    if there is nothing left in the character stream.

    # Errors

    If problems occur while trying to read from the stream then an `std::io::Error` variant is
    returned.

    # Examples

    `accept` is effectively the same as using `peek`, confirming that a specific character
    was found, and then using `read` to remove that character from the input stream. Where this is
    needed, `accept` will give you much tidier code. The example code shown for `peek` can
    become a little tidier by using `accept` instead of `peek` followed by `read`:

    ```
    # use sipp::{parser::Parser, decoder::Utf8Decoder,
    # decoder::ByteStreamCharDecoder};
    # fn main() -> Result<(), std::io::Error> {
    # let input = "<!-- comment found -->";
    # let decoder = Utf8Decoder::wrap(input.as_bytes());
    # let mut parser = Parser::wrap(decoder);
    if parser.accept('<')? {
        // Check what comes after the '<' character.
        match parser.peek()? {
            Some('!') => println!("This is either a comment or a CDATA section!"),
            Some('?') => println!("This is a processing instruction!"),
            Some(_) => println!("Nothing else matches, so this must be an XML element!"),
            None => println!("Input ends suddenly after the '<' character!"),
        }
    } else {
        println!("Some other type of content found!");
    }
    # Ok(())
    # }
    ```
    */
    pub fn accept(&mut self, c: char) -> Result<bool, Error> {
        match self.peek()? {
            Some(f) => {
                if f == c {
                    // Move past the peek character.
                    self.skip_next()?;
                    Ok(true)
                } else {
                    Ok(false)
                }
            }
            None => Ok(false),
        }
    }

    /**
    Requires that the next character in the input stream is identical to the given character,
    and removes it from the input stream.

    # Errors

    If the next character in the stream is not identical to the given character, or if there is
    no more content available from the stream, then an `std::io::Error` variant is returned.

    # Examples

    'require` is intended for use when your parser is in a state such that only one specific
    character is legally permitted by the specification of the type of document you're reading.
    For example, if you're reading the assignment part of a key-value pair in an XML attribute,
    you might use something like this:

    ```
    # use std::io::{Error, ErrorKind};
    # use sipp::{parser::Parser, decoder::Utf8Decoder,
    # decoder::ByteStreamCharDecoder};
    # fn main() -> Result<(), std::io::Error> {
    # let input = "='v'";
    # let decoder = Utf8Decoder::wrap(input.as_bytes());
    # let mut parser = Parser::wrap(decoder);
    // We've already read the name of the attribute, so now we require an equals sign, followed by
    // either a double-quote or single-quote character.
    parser.require('=')?;
    let quote_character = match parser.read()? {
        '\'' => '\'',
        '"' => '"',
        _ => return Err(Error::new(ErrorKind::InvalidData, "Badly formed attribute!")),
    };
    // Now read the actual value of the attribute. (For simplicity, in this example it's just one
    // character, so we'll use `read` and do nothing with the value at all.)
    let _attribute_value = parser.read()?;
    // Now we require that the next character must be the same sort of quote character that opened
    // the attribute value. Anything else would mean that the input was invalid, so an error will
    // be generated.
    parser.require(quote_character)?;
    # Ok(())
    # }
    ```
    */
    pub fn require(&mut self, c: char) -> Result<(), Error> {
        match self.read_next()? {
            Some(f) => {
                if f == c {
                    Ok(())
                } else {
                    Err(Error::new(
                        ErrorKind::InvalidData,
                        format!("Expected character '{}' but found '{}'!", c, f),
                    ))
                }
            }
            None => Err(Error::new(
                ErrorKind::InvalidData,
                format!("Input ends without expected character '{}'!", c),
            )),
        }
    }

    /**
    Requires that the next sequence of characters read (and removed) from the input stream
    exactly matches the sequence of characters found in the given `str`.

    # Errors

    If the next characters from the stream do not exactly match the given `str`, or if there is
    no more content available from the stream, then an `std::io::Error` variant  is returned.

    # Examples

    `require_str` is intended for use when your parser is in a state such that only one specific
    character sequence is legally permitted by the specification of the type of document you're
    reading. For example, if you're reading the XML declaration at the start of an XML document and
    you know that `<?xml` must occur, then some whitespace, and then `version=` must come next, then
    you could use something like this:
    ```
    # use std::io::{Error, ErrorKind};
    # use sipp::{parser::Parser, decoder::Utf8Decoder,
    # decoder::ByteStreamCharDecoder};
    # fn main() -> Result<(), std::io::Error> {
    # let input = "<?xml version='1.0' encoding='UTF-8'?>";
    # let decoder = Utf8Decoder::wrap(input.as_bytes());
    # let mut parser = Parser::wrap(decoder);
    parser.require_str("<?xml")?;
    // In realitiy we'd need to skip a variety of potential whitespace characters, but for
    // simplicity in this example, let's just require that a space character comes next.
    parser.require(' ')?;
    parser.require_str("version=")?;
    # Ok(())
    # }
    ```
    */
    pub fn require_str(&mut self, s: &str) -> Result<(), Error> {
        for c in s.chars() {
            match self.read_next()? {
                None => {
                    return Err(Error::new(
                        ErrorKind::InvalidData,
                        format!(
                            "Input ends before completion of required sequence \"{}\"!",
                            s
                        ),
                    ));
                }
                Some(f) => {
                    if f != c {
                        return Err(Error::new(
                        ErrorKind::InvalidData,
                        format!(
                            "Found character '{}' but expected '{}', to satisfy required sequence \"{}\"!",
                            f, c, s
                        ),
                    ));
                    }
                }
            }
        }
        Ok(())
    }

    /**
    Removes from the input stream everything up to but **not including** the given character,
    and returns a `String` which holds any characters found before that given character.
    If this method is called when there is nothing left in the character stream, or when the
    very next character in the stream is the given character, then `None` will be returned.

    # Errors

    If any problem occurs while reading from the stream then an `std::io::Error` variant will be
    returned.

    # Examples

    `read_up_to` is useful when you simply want to capture all of the content up to but not
    including some terminator character (or the end of the input stream). For example, if you
    know that your parser application requires a double-quoted sequence then the following would
    extract everything between the double-quotes for you.

    ```
    # use std::io::{Error, ErrorKind};
    # use sipp::{parser::Parser, decoder::Utf8Decoder,
    # decoder::ByteStreamCharDecoder};
    # fn main() -> Result<(), std::io::Error> {
    # let input = "\"Some text found within double-quotes!\"";
    # let decoder = Utf8Decoder::wrap(input.as_bytes());
    # let mut parser = Parser::wrap(decoder);
    parser.require('"')?;
    if let Some(quoted_text) = parser.read_up_to('"')? {
        println!("Found within quotes: \"{}\".", quoted_text);
        # assert_eq!(quoted_text.as_str(), "Some text found within double-quotes!");
    } else {
        println!("Empty quotes!");
        # panic!("This should not happen!!!");
    }
    // Because `read_up_to` does not consume the specified terminator character,
    // we need to use `require` to confirm that it does appear as expected.
    parser.require('"')?;
    # Ok(())
    # }
    ```

    `read_up_to` is also useful if the input stream might end without a delimiter/terminator
    character, such as a comma-separated list. An error will not be thrown if this method is
    called when no input remains, or if the end of input occurs before the given character is
    found. This means it's safe to use this method for reading a comma-separated list, where
    the final entry in the list will not be followed by a comma, just by the end of the input.

    ```
    # use std::io::{Error, ErrorKind};
    # use sipp::{parser::Parser, decoder::Utf8Decoder,
    # decoder::ByteStreamCharDecoder};
    # fn main() -> Result<(), std::io::Error> {
    let input = "comma after,comma after,still a comma after,input ends after this entry";
    let decoder = Utf8Decoder::wrap(input.as_bytes());
    let mut parser = Parser::wrap(decoder);
    let mut data = Vec::with_capacity(4);
    // Keep reading while input is available.
    while parser.has_more()? {
        // Read everything up to the next comma (or the end of input, whichever comes first).
        if let Some(value) = parser.read_up_to(',')? {
            data.push(value);
        }
        // Skip past a comma, if one is found (end of input may be found instead).
        parser.accept(',')?;
    }
    # assert_eq!(data, vec!["comma after", "comma after", "still a comma after",
            "input ends after this entry"]);
    # Ok(())
    # }
    ```
    */
    pub fn read_up_to(&mut self, stop_before: char) -> Result<Option<String>, Error> {
        match self.peek()? {
            None => Ok(None),
            Some(c) => {
                if c == stop_before {
                    return Ok(None);
                }
                let mut s = String::with_capacity(16);
                s.push(c);
                self.skip_next()?;
                while let Some(c) = self.peek()? {
                    if c == stop_before {
                        break;
                    } else {
                        self.skip_next()?;
                        s.push(c);
                    }
                }
                s.shrink_to_fit();
                Ok(Some(s))
            }
        }
    }

    /**
    Removes characters from the input stream which satisfy a predicate function, and stops as soon
    as the next character would not satisfy the function. Returns `true` if any acceptable
    characters were found and skipped; otherwise returns `false`. If there is no content
    available in the input stream then `false` will be returned.

    # Errors

    If any other problems occur while reading from the stream, then an `std::io::Error` variant
    will be returned.

    # Examples

    `skip_while` is most useful for skipping sequences of insignificant characters such as
    whitespace, where you may need to know whether or not the character type occurs, but you
    don't actually care which specific characters or how many characters occur. For example, if
    you want to confirm that whitespace occurs (because you require some sort of whitespace as a
    separator) but don't care what specific whitespace sequence is used (spaces, tabs, newlines,
    whatever) then the following would do the job.

    ```
    # use std::io::{Error, ErrorKind};
    # use sipp::{parser::Parser, decoder::Utf8Decoder,
    # decoder::ByteStreamCharDecoder};
    # fn main() -> Result<(), std::io::Error> {
    # let input = "' \n\t next=";
    # let decoder = Utf8Decoder::wrap(input.as_bytes());
    # let mut parser = Parser::wrap(decoder);
    // Read the end of a single-quoted value.
    parser.require('\'')?;
    // Now check whether there's some/any sort of "ASCII" whitespace.
    let found_whitespace = parser.skip_while(|c| c.is_ascii_whitespace())?;
    // Return an error if whitespace was not found (because we require whitespace after a quoted
    // value).
    if !found_whitespace {
        return Err(Error::new(ErrorKind::InvalidData, "Whitespace required after attribute!"));
    }
    // Now check for whatever is expected after the whitespace.
    parser.require_str("next")?;
    # Ok(())
    # }
    ```
    */
    pub fn skip_while<P>(&mut self, acceptable: P) -> Result<bool, Error>
    where
        P: Fn(char) -> bool,
    {
        let mut found = false;
        while let Some(c) = self.peek()? {
            if !acceptable(c) {
                break;
            }
            self.skip_next()?;
            found = true;
        }
        Ok(found)
    }
}

#[cfg(test)]
mod tests {
    use crate::decoder::Utf8Decoder;

    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    type TestOut = Result<(), Error>;

    #[test]
    fn read_empty() -> TestOut {
        let input = "";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        let outcome = parser.read();
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn read_first_character() -> TestOut {
        let input = "a";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        let result = parser.read()?;
        assert_eq!(result, 'a');
        Ok(())
    }

    #[test]
    fn read_all_characters() -> TestOut {
        let input = "abc123<*>";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        assert_eq!(parser.read()?, 'a');
        assert_eq!(parser.read()?, 'b');
        assert_eq!(parser.read()?, 'c');
        assert_eq!(parser.read()?, '1');
        assert_eq!(parser.read()?, '2');
        assert_eq!(parser.read()?, '3');
        assert_eq!(parser.read()?, '<');
        assert_eq!(parser.read()?, '*');
        assert_eq!(parser.read()?, '>');
        let outcome = parser.read();
        assert!(outcome.is_err());
        Ok(())
    }

    #[test]
    fn require_empty() -> TestOut {
        let input = "";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        assert!(parser.require('i').is_err());
        Ok(())
    }

    #[test]
    fn require_first_character_of_input() -> TestOut {
        let input = "i";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        assert!(parser.require('i').is_ok());
        Ok(())
    }

    #[test]
    fn require_last_character_of_input() -> TestOut {
        let input = "abci";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        assert_eq!(parser.read()?, 'a');
        assert_eq!(parser.read()?, 'b');
        assert_eq!(parser.read()?, 'c');
        assert!(parser.require('i').is_ok());
        Ok(())
    }

    #[test]
    fn require_end_of_input() -> TestOut {
        let input = "i";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        assert_eq!(parser.read()?, 'i');
        assert!(parser.require('D').is_err());
        Ok(())
    }

    #[test]
    fn peek_empty() -> TestOut {
        let input = "";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        assert_eq!(parser.peek()?, None);
        Ok(())
    }

    #[test]
    fn peek_exists_first_character() -> TestOut {
        let input = "a";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        assert_eq!(parser.peek()?, Some('a'));
        Ok(())
    }

    #[test]
    fn peek_exists() -> TestOut {
        let input = "input:a";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        parser.require_str("input:")?;
        assert_eq!(parser.peek()?, Some('a'));
        Ok(())
    }

    #[test]
    fn peek_no_more_input() -> TestOut {
        let input = "input:";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        parser.require_str("input:")?;
        assert_eq!(parser.peek()?, None);
        Ok(())
    }

    #[test]
    fn require_exact_empty() -> TestOut {
        let input = "";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        assert!(parser.require_str("i").is_err());
        assert!(parser.require_str("sequence").is_err());
        Ok(())
    }

    #[test]
    fn require_exact_last_character_of_input() -> TestOut {
        let input = "i";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        assert!(parser.require_str("i").is_ok());
        assert!(parser.require_str("string").is_err());
        assert_eq!(parser.peek()?, None);
        Ok(())
    }

    #[test]
    fn require_exact_end_of_input() -> TestOut {
        let input = "iD";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        assert!(parser.require_str("iD").is_ok());
        assert!(parser.require_str("KFA").is_err());
        assert!(parser.require_str("™").is_err());
        assert_eq!(parser.peek()?, None);
        Ok(())
    }

    #[test]
    fn peek_finds_empty() -> TestOut {
        let input = "";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        assert!(!parser.peek_sees('a')?);
        Ok(())
    }

    #[test]
    fn peek_finds_exists_first_position() -> TestOut {
        let input = "a";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        assert!(parser.peek_sees('a')?);
        Ok(())
    }

    #[test]
    fn peek_finds_exists() -> TestOut {
        let input = "000a";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        parser.require_str("000")?;
        assert!(parser.peek_sees('a')?);
        Ok(())
    }

    #[test]
    fn peek_finds_absent() -> TestOut {
        let input = "b";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        assert!(!parser.peek_sees('a')?);
        Ok(())
    }

    #[test]
    fn peek_finds_no_more_input() -> TestOut {
        let input = "bash";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        parser.require_str("bash")?;
        assert!(!parser.peek_sees('a')?);
        Ok(())
    }

    #[test]
    fn find_and_take_empty() -> TestOut {
        let input = "";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        assert!(!parser.accept('a')?);
        Ok(())
    }

    #[test]
    fn find_and_take_exists_first_character() -> TestOut {
        let input = "a";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        assert!(parser.accept('a')?);
        assert_eq!(parser.peek()?, None);
        Ok(())
    }

    #[test]
    fn find_and_take_exists() -> TestOut {
        let input = "prelude:a";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        parser.require_str("prelude:")?;
        assert!(parser.accept('a')?);
        assert_eq!(parser.peek()?, None);
        Ok(())
    }

    #[test]
    fn find_and_take_absent() -> TestOut {
        let input = "b";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        assert!(!parser.accept('a')?);
        Ok(())
    }

    #[test]
    fn find_and_take_no_more_input() -> TestOut {
        let input = "b";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        parser.require('b')?;
        assert!(!parser.accept('a')?);
        Ok(())
    }

    #[test]
    fn skip_while_whitespace() -> TestOut {
        let input = "\r\n\t   Some actual text";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        assert!(parser.skip_while(|c| c.is_ascii_whitespace())?);
        assert_eq!(parser.peek()?, Some('S'));
        Ok(())
    }

    #[test]
    fn skip_while_everything() -> TestOut {
        let input = "text";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        assert!(parser.skip_while(|c| c.is_ascii_alphabetic())?);
        assert_eq!(parser.peek()?, None);
        Ok(())
    }

    #[test]
    fn skip_while_after_entire_content() -> TestOut {
        let input = "start";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        parser.require_str("start")?;
        assert!(!parser.skip_while(|c| c.is_whitespace())?);
        Ok(())
    }

    #[test]
    fn read_up_to_empty() -> TestOut {
        let input = "";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        assert!(parser.read_up_to('a')?.is_none());
        Ok(())
    }

    #[test]
    fn read_up_to_absent() -> TestOut {
        let input = "abcde";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        assert_eq!(parser.read_up_to(';')?.unwrap(), input);
        Ok(())
    }

    #[test]
    fn read_up_to_first_character() -> TestOut {
        let input = "a";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        let nothingness = parser.read_up_to('a')?;
        assert!(&nothingness.is_none());
        Ok(())
    }

    #[test]
    fn read_up_to_next_character() -> TestOut {
        let input = "prelude:a";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        parser.require_str("prelude:")?;
        let nothingness = parser.read_up_to('a')?;
        assert!(&nothingness.is_none());
        Ok(())
    }

    #[test]
    fn read_up_to_exists() -> TestOut {
        let input = "&SomeCharRef;";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        parser.require('&')?;
        let char_ref_name = parser.read_up_to(';')?;
        assert_eq!(&char_ref_name.unwrap(), "SomeCharRef");
        parser.require(';')?;
        Ok(())
    }

    #[test]
    fn read_up_to_no_more_input() -> TestOut {
        let input = "&amp;";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        parser.require_str("&amp;")?;
        assert!(parser.read_up_to(';')?.is_none());
        Ok(())
    }

    #[test]
    fn has_more_start() -> TestOut {
        let input = "whatever";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        assert!(parser.has_more()?);
        Ok(())
    }

    #[test]
    fn has_more_mid() -> TestOut {
        let input = "whatever";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        parser.require_str("what")?;
        assert!(parser.has_more()?);
        Ok(())
    }

    #[test]
    fn has_more_end() -> TestOut {
        let input = "whatever";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        parser.require_str("whatever")?;
        assert!(!parser.has_more()?);
        Ok(())
    }

    #[test]
    fn has_more_empty() -> TestOut {
        let input = "";
        let mut parser = Parser::wrap(Utf8Decoder::wrap(input.as_bytes()));
        assert!(!parser.has_more()?);
        Ok(())
    }

    // TODO: ADD FURTHER TESTS !!!
}
